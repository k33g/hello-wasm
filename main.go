package main

import (
	"fmt"
	"syscall/js"
)

// HelloWorld returns an interface

func HelloWorld(this js.Value, args []js.Value) interface{} {
	// get the parameters
	firstName := args[0].String() 
	lastName := args[1].String()

	return map[string]interface{}{
		"message": "Hello " + firstName + " " + lastName,
		"author":  "@k33g_org",
	}
}

func main() {
	fmt.Println("Hello From GoLang")

	// Define the function in the JavaScript scope
	// Cast `HelloWorld` to JavaScript function with `js.FuncOf`
	js.Global().Set("HelloWorld", js.FuncOf(HelloWorld))

	// Prevent the function from returning, which is required in a wasm module
	select {}
}
